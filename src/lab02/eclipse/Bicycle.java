//Luke Weaver, 1937361
package lab02.eclipse;

public class Bicycle {
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;
	
	public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
		this.manufacturer = manufacturer;
		this.numberGears = numberGears;
		this.maxSpeed = maxSpeed;
	}
	
	public String getManufacturer(Bicycle b) {
		return this.manufacturer;
	}
	
	public int getGears(Bicycle b) {
		return this.numberGears;
	}
	
	public double getSpeed(Bicycle b) {
		return this.maxSpeed;
	}
	
	public String toString() {
		return "Manufacturer: " + manufacturer + 
				"\nNumber of Gears: " + numberGears + 
				"\nMax Speed: " + maxSpeed + 
				"\n";
	}
}
