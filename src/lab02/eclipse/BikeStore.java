//Luke Weaver, 1937361
package lab02.eclipse;

public class BikeStore {
	public static void main(String[] args) {
		Bicycle[] bikes = new Bicycle[4];
		bikes[0] = new Bicycle("James' Joints", 21, 40);
		bikes[1] = new Bicycle("Stephen's Spokes", 28, 45);
		bikes[2] = new Bicycle("Weaver's Wheels", 14, 35);
		bikes[3] = new Bicycle("Pomerantz's Pedals", 35, 50);
		
		for(int i = 0; i < bikes.length; i++) {
			System.out.println(bikes[i].toString());
		}
	}
}
